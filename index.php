<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
</head>
<body>
<div class="container">
    <div class="row">
        <?php
        ini_set("display_errors",1);
        error_reporting(E_ALL);





        if (isset($_GET['download'])) {
            $pieces = explode("/", $_GET['download']);
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.$pieces[2]);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($_GET['download']));
            ob_clean();
            flush();
            readfile($_GET['download']);
        }
        if (isset($_GET['dir'])) {
            $directory = $_GET['dir'];
        } else {
            $directory = "./";
        }
        $dir = opendir($directory);
        $skan_files = scandir($directory);

        ?>


        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <ul class="nav navbar-nav">
                    <li><a href="?">HOME</a> </li>
                </ul>
            </div>
        </nav>
        <div class="main" style="margin-top: 85px">

            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Size</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>

        <?php
        foreach ($skan_files as $file)
//        while ($file = readdir($dir))
        {
            if (isset($_GET['dir'])) {
                $file_type = $_GET['dir'].'/'.$file;
            }else{
                $file_type = $file;
            }
            $filedel = $file;

            echo "<tr>";

            if (is_dir($file_type))
            {

            if (isset($_GET['del'])) {
                if($file == $_GET['del']){
//                    $dirname = $_SERVER['DOCUMENT_ROOT'].'/'.$_GET['del'];
                      $dirname = $_GET['del'];
                      $skan_files = scandir($dirname);


                    function delete_directory($dirname) {
                        $skan_dir = scandir($dirname);
                        foreach ($skan_dir as $item){
                            if(is_dir($dirname.'/'.$item)){
                                unlink($dirname."/".$item);
                            }else{
                                delete_directory($dirname.'/'.$item);
                            }

                        }
                        rmdir($dirname);
                        return true;

                    }

                }
            }

                if (isset($_GET['dir'])) {
                    $filedel = $_GET['dir'] . '/' . $file;
                }else{
                    $filedel = $file;
                }

                if($file != '.'){
                    $directory = $file;
                    if (isset($_GET['dir'])) {
                        $directory = $_GET['dir'] . '/' . $file . '/';
                    }
                    echo '<td><span class="glyphicon glyphicon-folder-open"></span></td>';
                    echo '<td><a href="?dir='.$file_type.'">'.$file.'</a></td>';
                    echo '<td></td>';

                    if($file != '..'){
                        echo '<td><a href="?del='.$filedel.'"><button class="btn btn-danger">Delete</button></a></td>';
                    }else{
                        echo '<td></td>';
                    }
                    echo '<td></td>';
                }




            } else {

//                if (isset($_GET['del'])) {
//                    unlink($_GET['del']);
//                }

                $filename = $file;
                $filedownload = $file;
                if (isset($_GET['dir'])) {
                    $filedel = $_GET['dir'] . '/' . $file;
                    $filesize = filesize($_GET['dir'] . '/' . $file);
                    $filename = $_GET['dir'] . '/' . $file;
                    $filedownload = $_GET['dir'] . '/' . $file;
                }else{
                    $filesize = filesize($file);
                    $filedel = $file;
                }
                echo '<td><span class="glyphicon glyphicon-file"></span></td>';
                echo '<td><a href="'.$filename.'">'.$file.'</a></td>';
                echo "<td>$filesize bytes</td>";
                echo "<td><a href='?del=$filedel'><button class='btn btn-danger'>Delete</button></a></td>";
                echo "<td><a href='?download=$filedownload'><button class='btn btn-primary'>Download</button></a></td>";
            }
            echo "</tr>";

        }

        closedir($dir);
        ?>


                </tbody>
            </table>
        </div>
    </div>
</div>


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
